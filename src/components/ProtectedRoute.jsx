import React, { Component } from 'react'
import { Redirect } from 'react-router'

export default function ProtectedRoute( {isSignin, onSignin, component: Component, path}) {

    if(isSignin)
        return <Component path={path} onSignin={onSignin}/>
    else return <Redirect to="/auth"/>
}
