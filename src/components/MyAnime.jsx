import React, { useState } from 'react'
import { ButtonGroup, Button } from 'react-bootstrap'
import { Link, useLocation, useRouteMatch } from 'react-router-dom'

export default function MyAnime() {


    const [animes, setAnimes] = useState([ "Action", "Romance", "Comendy" ])

    const { url } = useRouteMatch()
    let location = useLocation()
    let query = new URLSearchParams(location.search)
    let value = query.get("type")

    return (
        <div>
            <h2>Animes</h2>
            <ButtonGroup toggle >
                {
                    animes.map((anime, index) => (
                        <Button key={index} as={Link} to={`${url}?type=${anime}`}
                            type="radio"
                            variant="secondary"
                        >{anime}
                        </Button>
                    ))
                }
            </ButtonGroup>
            <h3>Please Select Category:
             <span style={{ color: "red" }}>
                    {value}
                </span>
            </h3>
        </div>
    )
}
