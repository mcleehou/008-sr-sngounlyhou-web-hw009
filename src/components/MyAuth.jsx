import React from 'react'
import { Col, Form, Row, Button } from 'react-bootstrap'

export default function MyAuth({isSignin, onSignin}) {
    return (
        <div>
            <Row>
                <Col sm={6} className="mx-auto">
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                        </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" />
                        </Form.Group>
                        <Form.Group controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Check me out" />
                        </Form.Group>
                        <Button onClick={onSignin} variant="primary">
                            {isSignin?"Log out": "Log in"}
                        </Button>
                    </Form>
                </Col>
            </Row>
        </div>
    )
}
