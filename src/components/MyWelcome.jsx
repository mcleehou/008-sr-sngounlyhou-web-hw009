import React from 'react'
import { Button } from 'react-bootstrap'
export default function MyWelcome({path, onSignin}) {
    return (
        <div>
            Welcome <br />
            <Button onClick={onSignin} variant="primary"> Log out </Button>
        </div>
    )
}
