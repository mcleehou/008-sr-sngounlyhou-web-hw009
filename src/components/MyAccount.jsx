import React from 'react'
import { Route, Switch } from 'react-router'
import { BrowserRouter as Router, Link, useRouteMatch } from 'react-router-dom'
import MyAccountType from './MyAccountType'

export default function MyAccount() {

    let { url } = useRouteMatch()

    return (
        <div>
            <Router>
                <h2>Account</h2>
                <ul>
                    <li><Link to={`${url}/netflix`}>NetFlix</Link></li>
                    <li><Link to={`${url}/zillow-group`}>Zillow Group</Link></li>
                    <li><Link to={`${url}/yahoo`}>Yahoo</Link></li>
                    <li><Link to={`${url}/modus-create`}>Modus Create</Link></li>
                </ul>
                <Switch>
                    <Route path="/account/:type" render={()=><MyAccountType />} />
                </Switch>
            </Router>
        </div>
    )
}
