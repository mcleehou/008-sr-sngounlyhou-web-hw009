import React, { useState } from 'react'
import { Card, Col, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function MyCard() {

    const [movies, setMovies] = useState([
        {
            id: 1,
            title: "Minion",
            content: "Content",
            image: "./images/minion.jpeg"
        },
        {
            id: 2,
            title: "Minion",
            content: "Content",
            image: "./images/minion.jpeg"
        },
        {
            id: 3,
            title: "Minion",
            content: "Content",
            image: "./images/minion.jpeg"
        },
        {
            id: 4,
            title: "Minion",
            content: "Content",
            image: "./images/minion.jpeg"
        },
        {
            id: 5,
            title: "Minion",
            content: "Content",
            image: "./images/minion.jpeg"
        },
        {
            id: 6,
            title: "Minion",
            content: "Content",
            image: "./images/minion.jpeg"
        }
    ])

    return (
        <div>
            <Row>
            {
                movies.map((movie, index) => (
                    <Col sm={3} key={index}>
                    <Card style={{ margin: '5px 0' }} >
                        <Card.Img style={{ height: '10rem' }} variant="top" src={movie.image} />
                        <Card.Body>
                            <Card.Title>{ movie.title }</Card.Title>
                            <Card.Text>
                                { movie.content}
                            </Card.Text>
                            <Link to={`/detail/${movie.id}`} className="btn btn-primary">Read</Link>
                        </Card.Body>
                    </Card>
                    </Col>
                ))
            }
            </Row>
        </div>
    )
}
