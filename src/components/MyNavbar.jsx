import React from 'react'
import { Nav, Navbar, Button, Form, FormControl } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function MyNavbar() {
    return (
        <div>
            <Navbar bg="light" expand="lg">
            <div className="container">
                <Navbar.Brand href="#">Navbar scroll</Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                        className="mr-auto me-2 my-lg-0"
                        style={{ maxHeight: '100px' }}
                    >
                        <Nav.Link as={ Link } to="/">Home</Nav.Link>
                        <Nav.Link as={ Link } to="/video">Video</Nav.Link>
                        <Nav.Link as={ Link } to="/account">Account</Nav.Link>
                        <Nav.Link as={ Link } to="/welcome">Welcome</Nav.Link>
                        <Nav.Link as={ Link } to="auth">Auth</Nav.Link>
                        
                    </Nav>
                    <Form className="d-flex">
                        <FormControl
                            type="search"
                            placeholder="Search"
                            className="form-control me-2 "
                            aria-label="Search"
                        />
                        <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
            </div>
            </Navbar>
        </div>
    )
}
