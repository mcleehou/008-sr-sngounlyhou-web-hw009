import React, { useState } from 'react'
import { ButtonGroup, Button } from 'react-bootstrap'
import { Route, Switch, useRouteMatch } from 'react-router'
import { BrowserRouter as Router, Link } from 'react-router-dom'
import MyAnime from './MyAnime'
import MyMovies from './MyMovies'

export default function MyVideo() {


    let {url } = useRouteMatch()
    return (
        <div>
            <h2>Video</h2>
            <Router>
            <ButtonGroup toggle>
                    <Button as={Link} to={`${url}/movies`}
                        type="radio"
                        variant="secondary"
                        name="radio"
                        >Movies
                    </Button>

                    <Button as={Link} to={`${url}/animes`}
                        type="radio"
                        variant="secondary"
                        name="radio"
                        >Animes
                    </Button>
            </ButtonGroup>
            <Switch>
                <Route path={`${url}/animes`} render={()=> <MyAnime/>}/>
                <Route path={`${url}/movies`} render={()=> <MyMovies/>}/>
            </Switch>

            </Router>
        </div>
    )
}
