import React from 'react'
import { useParams } from 'react-router'

export default function MyAccountType() {

    let {type} = useParams()
    console.log(type);
    return (
        <div>
            <h3>ID: {type}</h3>
        </div>
    )
}
