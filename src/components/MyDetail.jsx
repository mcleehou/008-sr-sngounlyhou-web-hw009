import React from 'react'
import { useParams } from 'react-router'

export default function MyDetail() {

    const {id} = useParams()
    return (
        <div>
            Detial: {id}
        </div>
    )
}
