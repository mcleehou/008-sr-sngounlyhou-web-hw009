import React, { useState } from 'react'
import { ButtonGroup, Button } from 'react-bootstrap'
import { Link, useLocation, useRouteMatch } from 'react-router-dom'

export default function MyMovies() {

    const [cates, setCates] = useState([ "Adventure", "Crime", "Action", "Romance", "Comendy" ])
    let { url } = useRouteMatch()

    let location = useLocation()
    let query = new URLSearchParams(location.search)
    let value = query.get("type")

    return (
        <div>
            <h2>Movies</h2>
            <ButtonGroup toggle >
                {
                    cates.map((cate, index) => (
                        <Button key={index} as={Link} to={`${url}?type=${cate}`}
                            type="radio"
                            variant="secondary"
                            name="radio"
                        >{cate}
                        </Button>
                    ))
                }
            </ButtonGroup>
            <h3>Please Select Category:
             <span style={{ color: "red" }}>
                    {value}
                </span>
            </h3>
        </div>
    )
}
