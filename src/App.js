import React, { useState } from 'react'
import { Container } from 'react-bootstrap'
import MyNavbar from './components/MyNavbar'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import MyCard from './components/MyCard'
import MyVideo from './components/MyVideo'
import MyAccount from './components/MyAccount'
import MyWelcome from './components/MyWelcome'
import MyAuth from './components/MyAuth'
import MyDetail from './components/MyDetail'
import ProtectedRoute from './components/ProtectedRoute'


export default function App() {

    const [isSignin, setIsSignin] = useState(false)

    let onSignin = () =>{
        setIsSignin(!isSignin)
        console.log(isSignin);
    }
    

    return (
        <div>
            <Router>
            <MyNavbar />
                <Container>
                    <Switch>
                        <Route exact path="/">
                            <MyCard/>
                        </Route>
                        <Route path="/detail/:id">
                            <MyDetail />
                        </Route>
                        <Route path="/video">
                            <MyVideo />
                        </Route>
                        <Route path="/account">
                            <MyAccount />
                        </Route>
                        <ProtectedRoute isSignin={isSignin} onSignin={onSignin} component={MyWelcome} path="/welcome"/>
                        <Route  path="/auth" render={()=> <MyAuth onSignin={onSignin} isSignin={isSignin}/>}/>
                    </Switch>
                </Container>
            </Router>
        </div>
    )

}
